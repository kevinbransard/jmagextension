
// Traverse the bookmark tree, and print the folder and nodes.
function initBookmarklet(query) {
  console.log("LOADED",chrome.tabs);
  var tabUrl = "";
  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    tabUrl = tabs[0].url;
  });

  $.ajax({
    url: "https://intranet.jalios.net/jcms/jn1_44918/fr/espace-r-d?portlet=jn1_123596",
    data: {
      "url": tabUrl,
      "modal": "plugins/SocialBookmarkPlugin/jsp/addWebPage.jsp"
    }
  }).done(function(data) {
    $(".bookmarkletbody").append(data);
  });
}


document.addEventListener('DOMContentLoaded', function () {
  initBookmarklet();
});
